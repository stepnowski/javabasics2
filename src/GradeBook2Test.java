public class GradeBook2Test 
{
	public static void main(String[] args) 
	{
		// create a GradeBook1 object  and assign it to myGradeBook1
		GradeBook2 myGradeBook2 = new GradeBook2();
		
		// call myGradeBook's displayMessage method
		myGradeBook2.displayMessage();

	}// end method main
}// end class GradeBook2Test